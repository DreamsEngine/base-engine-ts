# Base Engine TS

<!-- [![npm version](https://badge.fury.io/js/starbase.svg)](https://badge.fury.io/js/starbase) -->

[![dependencies status](https://david-dm.org/DreamsEngine/base-engine-ts/status.svg)](https://david-dm.org/DreamsEngine/base-engine-ts)
[![FOSSA Status](https://app.fossa.com/api/projects/git%2Bgithub.com%2FDreamsEngine%2Fbase-engine-ts.svg?type=shield)](https://app.fossa.com/projects/git%2Bgithub.com%2FDreamsEngine%2Fbase-engine-ts?ref=badge_shield)

Bolerplate based on [Starbase](https://github.com/bstaruk/starbase) adding TypeScript, because why not?, for overkilling small projects :P .

- [Node.js](https://github.com/nodejs/node) & [Yarn](https://github.com/yarnpkg)
- [webpack 4](https://github.com/webpack/webpack) & [webpack-dev-server](https://github.com/webpack/webpack-dev-server)
- [TS Loader](https://github.com/TypeStrong/ts-loader) w/ [ESLint](https://github.com/eslint/eslint)
- [PostCSS](https://github.com/postcss/postcss) w/
  - [PostCSS Preset Env](https://github.com/csstools/postcss-preset-env)
  - [PostCSS Nested](https://github.com/postcss/postcss-nested)
  - [postcss-extend](https://github.com/travco/postcss-extend)
  - [stylelint](https://github.com/stylelint/stylelint)
  - [cssnano](https://github.com/ben-eb/cssnano)
  - [MQPacker](https://github.com/hail2u/node-css-mqpacker)
- ...and more!
